﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.EventSystems;

public class EvolutionCardController : MonoBehaviour, IPointerClickHandler {
	#region PublicFields
	public Action OnDrawCallback;
	public Action OnDiscardCallback;
	public Action OnPlayerDropCallback;
	public Action OnComputerDropCallback;

	public EvolutionCard CardData{
		get{
			return _cardData;
		}
		set{
			_cardData = value;
		}
	}

	#endregion
	#region PrivateFields
	private EvolutionCard _cardData;

	public EvolutionSkill skill;
	
	[Space]
	[Header("UI Elements")]
	public Text _cardNameText;
	public Text _cardNameText2;
	public Text _playerAdaptability;
	public Text _playerFerocity;
	public Text _playerDurability;
	public Text _computerAdaptability;
	public Text _computerFerocity;
	public Text _computerDurability;
	public Image _cardArt;
	#endregion

	#region UnityMethods
	private void Awake(){
	}
	private void Start(){
		//_cardData.Init(this);
	}
	private void Update(){
	}

    public void OnPointerClick(PointerEventData eventData)
    {
		if (_cardNameText2!=null)
		{
        FindObjectOfType<GameMaster>().OnCardDraft(this);
		}
		else if (FindObjectOfType<GameMaster>().handInactiveCards.Count>0)
		{
			FindObjectOfType<GameMaster>().DiscardCard(this);
		}
		else
		{
		FindObjectOfType<GameMaster>().OnCardUse(this);
		}
    }
    #endregion

    #region PublicMethods
    #endregion
    #region PrivateMethods
    #endregion
}
